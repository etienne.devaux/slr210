package com.example;

public class GatherMsg {
	public int id;
	public int ballot;
	public int imposeballot;
	public int estimate;
	
	public GatherMsg(int id,int b,int i,int e) {
		this.id = id;
		this.ballot = b;
		this.imposeballot = i;
		this.estimate = e;
	}
}
