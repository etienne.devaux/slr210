package com.example;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Main {

	public static int N = 100;
	public static int f = 49;
	public static long tle = 2000;

	public static void main(String[] args) throws InterruptedException {

		// Instantiate an actor system
		final ActorSystem system = ActorSystem.create("system");
		system.log().info("System started with N=" + N);

		ArrayList<ActorRef> references = new ArrayList<>();

		for (int i = 0; i < N; i++) { 
			// Instantiate processes
			final ActorRef a = system.actorOf(Process.createActor(i + 1, N), "" + i);
			references.add(a);
		}

		// give each process a view of all the other processes
		Members m = new Members(references);
		for (ActorRef actor : references) {
			actor.tell(m, ActorRef.noSender());
		}

		// shuffling and sending crash messages
		Collections.shuffle(references);
		for (int i = 0; i < f; i++) {
			references.get(i).tell(new CrashMsg(), ActorRef.noSender());
		}

		long starttime = System.currentTimeMillis();
		// sending launch messages
		for (int i = 0; i < N; i++) {
			references.get(i).tell(new StartTimeMsg(starttime),ActorRef.noSender());
			references.get(i).tell(new LaunchMsg((int)Math.floor(Math.random()*(1-0+1)+0)), ActorRef.noSender());
		}

		// sending hold messages
		Thread.sleep(tle);
		Random r = new Random();
		int leader = r.nextInt(N - f) + f;
		//system.log().info("Leader=" + leader);
		for (int i = f; i < N; i++) {
			if (i != leader)
				references.get(i).tell(new HoldMsg(), ActorRef.noSender());
		}

		try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}
}
