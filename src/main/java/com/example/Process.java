package com.example;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Process extends UntypedAbstractActor {

	static public class State {
		public final int estballot;
		public final int est;

		public State(int estballot, int est) {
			this.estballot = estballot;
			this.est = est;
		}
	}

	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);// Logger attached to actor
	private final int N;// number of processes
	private final int id;// id of current process
	private Members processes;// other processes' references
	private int proposal;
	private int ballot;
	private int readballot;
	private int imposeballot;
	private int estimate;
	private long starttime;
	private Boolean fault_prone = false;
	private Boolean silent = false;
	private Boolean hold = false;
	private Boolean decide = false;
	private HashMap<Integer, State> states;
	private double prob = 1;
	private int ackNumb = 0;

	public Process(int ID, int nb) {
		N = nb;
		id = ID;
		ballot = id - N;
		readballot = 0;
		imposeballot = id - N;
	}

	public String toString() {
		return "Process{" + "id=" + id;
	}

	/**
	 * Static function creating actor
	 */
	public static Props createActor(int ID, int nb) {
		return Props.create(Process.class, () -> {
			return new Process(ID, nb);
		});
	}

	public void propose(int v) {
		if (hold)
			return;
		proposal = v;
		ballot += N;
		states = new HashMap<Integer, State>();
		log.info("p" + getSelf().path().name() + " proposes " + v);

		for (ActorRef a : processes.references) {
			a.tell(new ReadMsg(ballot), getSelf());
			// log.info("Read ballot " + ballot + " msg: p" + self().path().name() + " -> p"
			// + a.path().name());
		}
	}

	private void readReceived(int newBallot, ActorRef pj) {
		if (readballot > newBallot || imposeballot > newBallot) {
			pj.tell(new AbortMsg(newBallot), getSelf());
		} else {
			readballot = newBallot;
			pj.tell(new GatherMsg(id, newBallot, imposeballot, estimate), this.getSelf());
		}
		// log.info("read received " + self().path().name());

	}

	public void onReceive(Object message) throws Throwable {
		if (!silent && fault_prone) {
			if (Math.random() < prob) {
				silent = true;
				//log.info("p" + self().path().name() + " is silent");
			}
		} else if (silent)
			return;

		else if (message instanceof Members) {// save the system's info
			Members m = (Members) message;
			processes = m;
			//log.info("p" + self().path().name() + " received processes info");

		} else if (message instanceof StartTimeMsg) {
			StartTimeMsg m = (StartTimeMsg) message;
			this.starttime = m.starttime;

		} else if (message instanceof ReadMsg) {
			ReadMsg m = (ReadMsg) message;
			this.readReceived(m.ballot, getSender());

		} else if (message instanceof CrashMsg) {
			fault_prone = true;
			//log.info("p" + this.getSelf().path().name() + " received crash message");

		} else if (message instanceof LaunchMsg) {
			if(!hold && !decide) {
				LaunchMsg m = (LaunchMsg) message;
				this.proposal = m.value;
				propose(m.value);
				Thread.sleep(5);
				getSelf().tell(new LaunchMsg(m.value), getSender());
			}

		} else if (message instanceof HoldMsg) {
			hold = true;
			log.info("p" + getSelf().path().name() + " received hold message");

		} else if (message instanceof AbortMsg) {
			//log.info("p" + self().path().name() + " aborted");
			return;
			
		} else if (message instanceof GatherMsg) {
			GatherMsg m = (GatherMsg) message;
			states.put(m.id, new State(m.estimate, m.ballot));
			if (states.size() > N / 2) {
				int max_estballot = 0;
				int est = 0;
				for (State s : states.values()) {
					if (s.estballot > max_estballot) {
						max_estballot = s.estballot;
						est = s.est;
					}
				}
				if (max_estballot > 0)
					proposal = est;
				states = new HashMap<Integer, State>();
				for (ActorRef a : processes.references) {
					a.tell(new ImposeMsg(ballot, proposal), getSelf());
				}
			}
		} else if (message instanceof ImposeMsg) {
			ImposeMsg m = (ImposeMsg) message;
			if (readballot > m.ballot || imposeballot > m.ballot) {
				getSender().tell(new AbortMsg(m.ballot), this.getSelf());
			} else {
				estimate = m.proposal;
				imposeballot = m.ballot;
				getSender().tell(new AckMsg(m.ballot), getSelf());
			}
		} else if (message instanceof AckMsg) {
			ackNumb++;
			if(this.ackNumb>N/2) {
				ackNumb = 0;
				for (ActorRef a : processes.references) {
					a.tell(new DecideMsg(proposal), getSelf());
				}
			}
		} else if (message instanceof DecideMsg) {
			DecideMsg m = (DecideMsg) message;
			if (!decide) {
				long endtime = System.currentTimeMillis();		
				log.info("p" + getSelf().path().name() + " decided " + m.proposal +" with consensus latency " + Long.toString((endtime - this.starttime)));
				for (ActorRef a : processes.references) {
					a.tell(m, getSelf());
				}
			}
			decide = true;
		}
	}
}
